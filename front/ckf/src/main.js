import { createApp } from 'vue'
import 'bootstrap'
import App from './App.vue'
import router from "./js/router";
import store from "./js/store";

const app = createApp(App);

app.use(router);
app.use(store);

app.mount("#app")
