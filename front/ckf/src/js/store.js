import { createStore } from 'vuex'

const store = createStore({
    state() {
        return {
            size: 0,
            files: []
        };
    },
    mutations: {
        clearFiles(state) {
            state.files = [];
            state.size = 0;
        },
        addFile(state, file) {
            if (state.size > 3000000) {
                console.log("Stored files size overhelms 3MB restriction. Clean files first.")
                return;
            }

            state.size += file.size;
            state.files.push(file);
        }
    },
    getters: {
        getFiles(state) {
            return state.files;
        },
        getSize(state) {
            return state.size;
        }
    }
});

export default store;