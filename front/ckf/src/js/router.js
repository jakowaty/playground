import  { createRouter, createWebHistory } from 'vue-router'

import Cropper from "../components/Cropper";
import Home from "../components/Home";
import Upload from "../components/Upload";

const cropper = Cropper;
const home = Home;
const upload = Upload;
const routes = [
    { path: '/', component: home },
    { path: '/crop', component: cropper },
    { path: '/upload', component: upload }
];

const router = createRouter({
    history: createWebHistory(),
    base: __dirname,
    routes: routes
});

export default router;