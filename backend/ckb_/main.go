package main

import "net/http"

func main() {
	mux := http.NewServeMux()

	mux.HandleFunc("/api/file_data", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			errorHandler(w, r, http.StatusNotFound)
		}
	})

	mux.HandleFunc("/health-check", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("ALIVE"))
	})

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		errorHandler(w, r, http.StatusNotFound)
	})

	http.ListenAndServe("127.0.0.1:9000", mux)
}

func errorHandler(w http.ResponseWriter, r *http.Request, status int) {
	w.WriteHeader(status)
	w.Write([]byte("NOT FOUND"))
}
