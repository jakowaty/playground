package input

import (
	"fmt"
	"regexp"
    "strconv"
)

func CheckIP(ip string) bool {
	matches, _ := regexp.MatchString(`^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}`, ip)

	return matches
}

func CheckPort(port string) bool {
	p, err := strconv.Atoi(port)

	if err != nil {
		fmt.Println(err)
		return false
	}

	if p < 1 || p > 65535 {
		return false
	}

	return true
}