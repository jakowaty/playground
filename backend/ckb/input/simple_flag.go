package input

import "strings"

const FlagPrefix = "--"

type Flag struct {
	FlagName string
	FlagCandidate string
}

func (f Flag) isFlag() bool {
	if !strings.HasPrefix(f.FlagCandidate, FlagPrefix) {
		return false
	}

	trimmedFlag := strings.Split(f.FlagCandidate[2:], "=")

	if len(trimmedFlag) != 2 {
		return false
	}

	if trimmedFlag[0] != f.FlagName {
		return false
	}

	return true
}

func (f Flag) GetFlagValue() string {
	if !f.isFlag() {
		panic("Not a flag")
	}

	trimmedFlag := strings.Split(f.FlagCandidate[2:], "=")

	return trimmedFlag[1]
}