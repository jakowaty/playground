package directory

import (
	"io/fs"
	"os"
)

func ScanDir(directory string, pattern string) []string {
	if _, err := os.Stat(directory); err != nil {
		panic("Not a valid path: " + directory)
	}

	dirFs := os.DirFS(directory)
	files, _ := fs.Glob(dirFs, pattern)

	if len(files) <= 0 {
		panic("No json files in formats directory")
	}

	for index, value := range files {
		files[index] = directory + "/" + value
	}

	return files
}