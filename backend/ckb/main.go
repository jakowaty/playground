package main

import (
	"fmt"
	"net/http"
	"os"
	"piotrbe.pl/go_any-api/input"
	"piotrbe.pl/go_any-api/directory"
	"piotrbe.pl/go_any-api/http_server"
)

func main() {
	defer func () {
		if err := recover(); err != nil {
			fmt.Println(err)
			fmt.Println("Exiting... something went wrong. Errors should be printed")
			os.Exit(255)
		}
	}()

	if len(os.Args) != 3 {
		fmt.Println("Usage: go_any-api <ADDR> <PORT>.")
		os.Exit(1)
	}

    if !input.CheckIP(os.Args[1]) {
        fmt.Println("Invalid IP format")
        os.Exit(2)
    }

    if !input.CheckPort(os.Args[2]) {
    	fmt.Println("Port must be number in proper range 1-65535")
		os.Exit(2)
	}

	// experiment with configs
	var configs []http_server.AnyApiServerConfig
	config1 := http_server.AnyApiServerConfig{
		Url: "/api/v1/health-check",
		Method: "GET",
		Callable: func(writer http.ResponseWriter, request *http.Request) {
			writer.Write([]byte("[\"ALIVE\"]"))
		},
		Headers: map[string]string{"Content-Type": "application/json"},
	}

	config2 := http_server.AnyApiServerConfig{
		Url:      "/api/v1/file_data",
		Method:   "POST",
		Callable: func(writer http.ResponseWriter, request *http.Request) {
			writer.Write([]byte("[\"ALIVE\"]"))
		},
		Headers: map[string]string{"Content-Type": "application/json"},
	}
	configs = append(configs, config1, config2)

	server := http_server.AnyApiServer{ Addr: os.Args[1], Port: os.Args[2], Config: configs }
	server.Serve()
}
