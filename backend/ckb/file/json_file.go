package file

import (
	"encoding/json"
	"os"
)

func ReadJSONFile(path string) {
	data, err := os.ReadFile(path)

	if err != nil {
		panic(err)
	}

	var jsonMap map[string]interface{}
	err2 := json.Unmarshal(data, &jsonMap)

	if err2 != nil {
		panic(err2)
	}
}