package http_server

import "net/http"

///////////////////////config and type of route controller///

type AnyApiServerConfig struct {
	Url      string
	Method   string
	Headers  map[string]string
	Callable AnyApiRouteFunc
}

type AnyApiRouteFunc func (writer http.ResponseWriter, request *http.Request)
