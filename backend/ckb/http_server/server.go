package http_server

import (
	"log"
	"net/http"
)


///////////////////////server and methods////////////////////

type AnyApiServer struct {
	Addr   string
	Port   string
	Config []AnyApiServerConfig
}

func (a AnyApiServer) Serve() {
	//var mappedConfig map[string]AnyApiServerConfig
	servAddr := a.Addr + ":" + a.Port
	mappedConfig := make(map[string]AnyApiServerConfig)

	for _, value := range a.Config {
		mappedConfig[value.Url] = value
	}

	log.Fatal(http.ListenAndServe(servAddr, AnyApiServerHandler{mappedConfig}))
}
