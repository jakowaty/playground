package http_server

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
)

///////////////////////handler///////////////////////////////

type AnyApiServerHandler struct {
	Config map[string]AnyApiServerConfig
}

func (f AnyApiServerHandler) routeMatch(request *http.Request, route AnyApiServerConfig) bool {
	result := true

	/**
	@Todo: add match route by pattern
	 */
	if strings.Compare(request.Method, route.Method) != 0 ||
		strings.Compare(request.RequestURI, route.Url) != 0 {
		result = false
	}

	return result
}

func (f AnyApiServerHandler) NotFound(writer http.ResponseWriter, request *http.Request) {
	writer.WriteHeader(http.StatusNotFound)
	writer.Header().Set("Content-Type", "application/json")

	json.NewEncoder(writer).Encode(map[string]string{
		"status" : strconv.Itoa(http.StatusNotFound),
		"url"	 : request.RequestURI,
		"method" : request.Method,
		"err"	 : "NOT FOUND",
	})

}

func (f AnyApiServerHandler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {

	//f.Config
	/*
	@Todo sorted matching
	 */
	for _, value := range f.Config {

		if f.routeMatch(request, value) {

			for key2, value2 := range value.Headers {
				writer.Header().Set(key2, value2)
			}

			value.Callable(writer, request)
			return
		}
	}


	f.NotFound(writer, request)
}
